<div class="menu trans_500">
    <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
        <div class="menu_close_container"><div class="menu_close"></div></div>
        <form action="#" class="menu_search_form">
            <input type="text" class="menu_search_input" placeholder="Search" required="required">
            <button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
        <ul>
            <li class="menu_item"><a href="/">Головна</a></li>
            <li class="menu_item"><a href="/about">Про нас</a></li>
            {{-- <li class="menu_item"><a href="services">Послуги</a></li> --}}
            <li class="menu_item"><a href="/pages/statti">Новини</a></li>
            <li class="menu_item"><a href="/contact">Контакти</a></li>
        </ul>
    </div>
    <div class="menu_social d-none">
        <ul>
            <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>