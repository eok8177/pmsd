<footer class="footer">
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/footer.jpg" data-speed="0.8"></div>
    <div class="footer_content">
        <div class="container">
            <div class="row">

                <!-- Footer About -->
                <div class="col-lg-3 footer_col">
                    <div class="footer_about">
                        <div class="logo">
                            <a href="/">ЦПМСД<span></span></a>    
                        </div>
                        <div class="footer_about_text">Наша місія – формування здорового способу життя та профілактика захворювань, забезпечення доступу до якісної медичної допомоги завдяки індивідуальному підходу, цінуючи Ваш час.</div>
                        <div class="footer_social d-none">
                            <ul class="d-flex flex-row align-items-center justify-content-start">
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="copyright"></div>
                    </div>
                </div>
                
                <!-- Footer Contact -->
                <div class="col-lg-5 footer_col">
                    <div class="footer_contact">
                        <div class="footer_contact_title">Написати нам</div>
                        <div class="footer_contact_form_container">
                            <form action="#" class="footer_contact_form" id="footer_contact_form">
                                <div class="d-flex flex-xl-row flex-column align-items-center justify-content-between">
                                    <input type="text" class="footer_contact_input" placeholder="Ім'я" required="required">
                                    <input type="email" class="footer_contact_input" placeholder="E-mail" required="required">
                                </div>
                                <textarea class="footer_contact_input footer_contact_textarea" placeholder="Повідомлення" required="required"></textarea>
                                <button class="footer_contact_button">Відправити</button>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Footer Hours -->
                <div class="col-lg-4 footer_col">
                    <div class="footer_hours">
                        <div class="footer_hours_title">Ми працюємо</div>
                        <ul class="hours_list">
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <div>Понеділок – П'ятниця</div>
                                <div class="ml-auto">8.00 – 18.00</div>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <div>Субота</div>
                                <div class="ml-auto">8.00 - 15.30</div>
                            </li>
                            <li class="d-flex flex-row align-items-center justify-content-start">
                                <div>Неділя</div>
                                <div class="ml-auto">вихідний</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bar">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer_bar_content d-flex flex-sm-row flex-column align-items-lg-center align-items-start justify-content-start">
                        <nav class="footer_nav">
                            <ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                <li class="active"><a href="/">Головна</a></li>
                                <li><a href="/about">Про нас</a></li>
                                {{-- <li><a href="services">Послуги</a></li> --}}
                                <li><a href="/pages/statti">Новини</a></li>
                                <li><a href="/contact">Контакти</a></li>
                            </ul>
                        </nav>
                        <div class="footer_links d-none">
                            <ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                <li><a href="#">Help Desk</a></li>
                                <li><a href="#">Emergency Services</a></li>
                                <li><a href="#">Appointment</a></li>
                            </ul>
                        </div>
                        <div class="footer_phone ml-lg-auto">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>0-(5539)-2-61-17</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>