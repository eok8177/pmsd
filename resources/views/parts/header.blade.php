<header class="header" id="header">
    <div>
        <div class="header_top">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header_top_content d-flex flex-row align-items-center justify-content-start">
                            <div class="logo">
                                <a href="/"><span style="display: inline-block;width: 30px;vertical-align: middle;"><img src="/images/medic.png" alt="Голопристанський ЦПМСД" style="max-width: 100%;"></span>  Голопристанський ЦПМСД</a>
                            </div>
                            <div class="header_top_extra d-flex flex-row align-items-center justify-content-start ml-auto">
                                <div class="header_top_nav">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                                        <li><a href="http://ks.medportal.ua" target="_blank">Медичний портал</a></li>
                                        <li><a href="http://kherson.liky.ua/#/medicationInHospital/163" target="_blank">Доступні ліки</a></li>
                                    </ul>
                                </div>
                                <div class="header_top_phone">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span>0-(5539)-2-61-17</span>
                                </div>
                            </div>
                            <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_nav" id="header_nav_pin">
            <div class="header_nav_inner">
                <div class="header_nav_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
                                    <nav class="main_nav">
                                        <ul class="d-flex flex-row align-items-center justify-content-start">
                                            <li class=""><a href="/">Головна</a></li>
                                            <li><a href="/about">Про нас</a></li>
                                            {{-- <li><a href="services">Послуги</a></li> --}}
                                            <li><a href="/pages/statti">Новини</a></li>
                                            <li><a href="/contact">Контакти</a></li>
                                        </ul>
                                    </nav>
                                    <div class="search_content d-flex flex-row align-items-center justify-content-end ml-auto">
                                        <form action="#" id="search_container_form" class="search_container_form d-none">
                                            <input type="text" class="search_container_input" placeholder="Пошук" required="required">
                                            <button class="search_container_button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</header>