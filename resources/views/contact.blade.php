@extends('layout')

@push('styles')
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <link rel="stylesheet" type="text/css" href="css/contact_responsive.css">
@endpush

@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
    <script src="js/contact.js"></script>
@endpush

@section('content')
    
    <!-- Home -->

    <div class="home">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/contact.jpg" data-speed="0.8"></div>

        <!-- Header -->

        @include('parts.header')

        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Контакти</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Contact -->

    <div class="contact">
        <div class="container">
            <div class="row">

                <!-- Contact form -->
                <div class="col-lg-8 contact_col ml-auto mr-auto">
                    <div class="contact_form">
                        <div class="contact_title">Написати нам</div>
                        <div class="contact_form_container">
                            <form action="#" id="contact_form" class="contact_form">
                                <input type="text" id="contact_input" class="contact_input" placeholder="Ваше ім'я" required="required">
                                <input type="email" id="contact_email" class="contact_input" placeholder="Ваш E-mail" required="required">
                                <input type="text" id="contact_subject" class="contact_input" placeholder="Тема" required="required">
                                <textarea class="contact_input contact_textarea" id="contact_textarea" placeholder="Повідомлення" required="required"></textarea>
                                <button class="contact_button" id="contact_button">Відправити</button>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Make an Appointment -->
                <div class="col-lg-4 contact_col d-none">
                    <div class="info_form_container">
                        <div class="info_form_title">Make an Appointment</div>
                        <form action="#" class="info_form" id="info_form">
                            <select name="info_form_dep" id="info_form_dep" class="info_form_dep info_input info_select">
                                <option>Department</option>
                                <option>Department</option>
                                <option>Department</option>
                            </select>
                            <select name="info_form_doc" id="info_form_doc" class="info_form_doc info_input info_select">
                                <option>Doctor</option>
                                <option>Doctor</option>
                                <option>Doctor</option>
                            </select>
                            <input type="text" class="info_input" placeholder="Name" required="required">
                            <input type="text" class="info_input" placeholder="Phone No">
                            <button class="info_form_button">make an appointment</button>
                        </form>
                    </div>
                </div>

                <!-- contact info -->
                <div class="contact_info">
                    <div class="row">
                        <div class="col-lg-3 offset-lg-1">
                            <div class="contact_info_list">
                                <div class="contact_info_title">Контактна інформація</div>
                                <ul>
                                    <li>Поліклініка</li>
                                    <li><span>Адреса: </span>Ларіонова 23, Гола Пристань</li>
                                    <li><span>Email: </span>goprycpmsd@ukr.net</li>
                                    <li><span>Телефон: </span>0-(5539)-2-61-17</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="contact_info_list contact_info_list_2">
                                <ul>
                                    <li>Дитяча консультація</li>
                                    <li><span>Адреса: </span>Горького 8, Гола Пристань</li>
                                    <li><span>Email: </span>goprycpmsd@ukr.net</li>
                                    <li><span>Телефон: </span>0-(5539)-2-14-03</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="contact_info_list">
                                <div class="contact_info_title">Ми працюємо</div>
                                <ul>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Понеділок – П'ятниця</div>
                                        <div class="ml-auto">8.00 – 18.00</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Субота</div>
                                        <div class="ml-auto">8.00 - 15.30</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div>Неділя</div>
                                        <div class="ml-auto">вихідний</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Google Map -->

    <div class="contact_map">
        <!-- Google Map -->
        <div class="map">
            <div id="google_map" class="google_map">
                <div class="map_container">
                    {{-- <div id="map"></div> --}}
                    <iframe src="https://www.google.com/maps/d/embed?mid=1yF4GVCkdEzwX0Iq2z93xZydbyxMXixMN" width="1920" height="800" style="width: 100%;"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
