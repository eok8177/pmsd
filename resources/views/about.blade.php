@extends('layout')

@push('styles')
    <link rel="stylesheet" type="text/css" href="css/about.css">
    <link rel="stylesheet" type="text/css" href="css/about_responsive.css">
@endpush

@push('scripts')
    <script src="js/about.js"></script>
@endpush

@section('content')
    
    <!-- Home -->

    <div class="home-about">
        <!-- Header -->

        @include('parts.header')

    </div>

    <!-- About -->

    <div class="about">
        <div class="container">
            <div class="row about_row row-eq-height">
                <div class="col-lg-8">
                    <div class="logo">
                        <a href="/">Голопристанський <span>ЦПМСД</span></a>
                    </div>
                    <div class="about_text_highlight">Наш клієнт – наш партнер, який співпрацює з нами та довіряє нам. Працівники Центру – колегіальні в прийнятті рішень, лікарі – молоді, енергійні та готові до змін, зацікавлені в збереженні Вашого здоров’я.</div>
                    <div class="about_text">

                      <p>Комунальне некомерційне підприємство «Голопристанський центр первинної медико-санітарної допомоги» прагне бути лідером серед медичних закладів Голопристанського району та Херсонської області. Наша місія – формування здорового способу життя та профілактика захворювань, забезпечення доступу до якісної медичної допомоги завдяки індивідуальному підходу, цінуючи Ваш час.</p>
                      <p>Для досягнення нашої мети ми:</p>
                      <ul class="list">
                        <li>використовуємо індивідуальний підхід та забезпечуємо своєчасне, якісне та безпечне надання медичних послуг згідно з вимогами чинного законодавства України, стандартів у сфері охорони здоров’я ;</li>
                        <li>активно впроваджуємо здоровий спосіб життя в систему суспільних та персональних цінностей;</li>
                        <li>впроваджуємо клієнтський сервіс для підвищення задоволеності під час перебування в нашому закладі;</li>
                        <li>вдосконалюємо процеси управління закладом та якістю медичних послуг, підвищення рівня та кваліфікації співробітників закладу, виявляємо та усуваємо невідповідності у своїй роботі;</li>
                        <li>працюємо командою, кожний співробітник відчуває свою персональну причетність та відповідальність до розвитку закладу та результатів його роботи;</li>
                        <li>шукаємо працівників, внутрішні цінності яких співпадають з головним принципом пацієнторієнтованості;</li>
                        <li>старанно виконуємо свої зобов’язання для створення позитивної ділової репутації;</li>
                        <li>розвиваємо матеріально-технічну базу закладу.</li>
                      </ul>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="about_image"><img src="images/about_1.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Milestones -->

    <div class="milestones">
        <div class="container">
            <div class="row">

                <!-- Milestone -->
                <div class="col-lg-3 milestone_col">
                    <div class="milestone d-flex flex-row align-items-center justify-content-start">
                        <div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_7.svg" alt=""></div>
                        <div class="milestone_content">
                            <div class="milestone_counter" data-end-value="365">0</div>
                            <div class="milestone_text">Днів у році</div>
                        </div>
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-lg-3 milestone_col">
                    <div class="milestone d-flex flex-row align-items-center justify-content-start">
                        <div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_6.svg" alt=""></div>
                        <div class="milestone_content">
                            <div class="milestone_counter" data-end-value="25" data-sign-after="k">0</div>
                            <div class="milestone_text">Пацієнтів за рік</div>
                        </div>
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-lg-3 milestone_col">
                    <div class="milestone d-flex flex-row align-items-center justify-content-start">
                        <div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_8.svg" alt=""></div>
                        <div class="milestone_content">
                            <div class="milestone_counter" data-end-value="45">0</div>
                            <div class="milestone_text">Лікарів</div>
                        </div>
                        
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-lg-3 milestone_col">
                    <div class="milestone d-flex flex-row align-items-center justify-content-start">
                        <div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_9.svg" alt=""></div>
                        <div class="milestone_content">
                            <div class="milestone_counter" data-end-value="12" data-sign-after="k">0</div>
                            <div class="milestone_text">Аналізів</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- CTA -->

    <div class="cta">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/cta_1.jpg" data-speed="0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="cta_container d-flex flex-xl-row flex-column align-items-xl-start align-items-center justify-content-xl-start justify-content-center">
                        <div class="cta_content text-xl-left text-center">
                            <div class="cta_title">Запишіться на прийом до лікаря.</div>
                            <div class="cta_subtitle">Використовуючи медичний портал.</div>
                        </div>
                        <div class="button cta_button ml-xl-auto"><a href="http://ks.medportal.ua" target="_blank"><span>перейти</span><span>перейти</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Doctors -->

    <div class="doctors">
        <div class="doctors_image"><img src="images/doctors.jpg" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title">Наші лікарі</div>
                    {{-- <div class="section_subtitle">вибрати з</div> --}}
                </div>
            </div>
            <div class="row doctors_row">
            @foreach($doctors as $item)
                <!-- Doctor -->
                <div class="col-xl-3 col-md-6">
                    <div class="doctor">
                        <div class="doctor_image"><img src="{{$item->image}}" alt="{{$item->title}}"></div>
                        <div class="doctor_content">
                            <div class="doctor_name"><a href="#doctor_{{$item->id}}" data-toggle="modal">{{$item->title}}</a></div>
                            <div class="doctor_title">{{$item->work}}</div>
                            {{-- <div class="doctor_link"><a href="#">+</a></div> --}}
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="doctor_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="doctor_{{$item->id}}Label" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="doctor_{{$item->id}}Label">{{$item->title}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img src="{{$item->image}}" alt="{{$item->title}}" style="width: 100px; margin-left: 20px; float: right;">
                        {!! $item->text !!}
                      </div>
                    </div>
                  </div>
                </div>
            @endforeach
            </div>
            {{-- <div class="row">
                <div class="col">
                    <div class="button doctors_button ml-auto mr-auto"><a href="#"><span>показати всіх</span><span>показати всіх</span></a></div>
                </div>
            </div> --}}
        </div>
    </div>
@endsection
