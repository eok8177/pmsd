@extends('layout')

@push('styles')
<link rel="stylesheet" type="text/css" href="/css/news.css">
<link rel="stylesheet" type="text/css" href="/css/news_responsive.css">
@endpush

@push('scripts')
<script src="/js/news.js"></script>
@endpush

@section('content')

<!-- Home -->

<div class="home">
  <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/news.jpg" data-speed="0.8"></div>

  <!-- Header -->

  @include('parts.header')

  <div class="home_container">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="home_content">
            <div class="home_title">{{ $page->category->title }}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- News -->

<div class="news">
  <div class="container">
    <div class="row">

      <!-- News Posts -->
      <div class="col-lg-8">
        <div class="news_posts">
          <div class="news_post">
            {{-- <div class="news_post_image"><img src="{{ $page->image }}" alt="{{ $page->title }}"></div> --}}
            <div class="news_post_title"><a href="{{ $page->slug }}">{{ $page->title }}</a></div>
            {{-- <div class="news_post_info">{{ $page->created_at }}</div> --}}
            <div class="news_post_text">{!! $page->preview !!}</div>
            <div class="news_post_text">{!! $page->content !!}</div>
          </div>
        </div>
      </div>

      <!-- Sidebar -->
      <div class="col-lg-4">
        <div class="news_sidebar">

          <!-- Search -->
          <div class="sidebar_search">
            <form action="#" id="sidebar_search" class="sidebar_search">
              <input type="text" class="sidebar_search_input" placeholder="Пошук" required="required">
              <button class="sidebar_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </div>

          <!-- Latest News -->
          <div class="sidebar_latest">
            <div class="sidebar_title">Останні новини</div>
            <div class="sidebar_latest_container">
              @foreach($lastPages as $item)
                <!-- Latest News Post -->
                <div class="latest d-flex flex-row align-items-start justify-content-start">
                  <div><div class="latest_image"><img src="{{ $item->image }}" alt="{{ $item->title }}"></div></div>
                  <div class="latest_content">
                    <div class="latest_title"><a href="{{ $item->slug }}">{{ $item->title }}</a></div>
                    {{-- <div class="latest_info"> --}}
                      {{-- {{ $item->created_at }} --}}
                      {{-- <ul class="d-flex flex-row align-items-start justify-content-start">
                        <li><a href="#">by Jane Smith</a></li>
                        <li><a href="#">April 25, 2018</a></li>
                      </ul> --}}
                    {{-- </div> --}}
                    {{-- <div class="latest_comments"><a href="#">2 Comments</a></div> --}}
                  </div>
                </div>
              @endforeach
            </div>
          </div>

          <!-- Categories -->
          <div class="sidebar_categories">
            <div class="sidebar_title">Категорії</div>
            <div class="categories">
              <ul>
                @foreach($categories as $item)
                <li>
                  <a href="/pages/{{ $item->slug }}">
                    <div class="d-flex flex-row align-items-center justify-content-start">
                      <div>{{ $item->title }}</div>
                      <div class="ml-auto">({{ $item->pages()->count() }})</div>
                    </div>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
