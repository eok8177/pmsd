@extends('layout')

@section('content')

    <!-- Home -->

    <div class="home">
        <div class="background_image" style="background-image:url(images/index_hero.jpg)"></div>

        <!-- Header -->

        @include('parts.header')

        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="home_title">Медичні послуги, яким можна довіряти</div>
                            <div class="home_text">Ми прагнемо бути лідером серед медичних закладів Голопристанського району та Херсонської області. Наша місія – формування здорового способу життя та профілактика захворювань, забезпечення доступу до якісної медичної допомоги завдяки індивідуальному підходу, цінуючи Ваш час.</div>
                            {{-- <div class="button home_button"><a href="#"><span>Читати ще</span><span>Читати ще</span></a></div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Info Boxes -->

    <div class="info">
        <div class="container">
            <div class="row row-eq-height">
            @foreach($pages as $item)
                <div class="col-lg-4 info_box_col">
                    <div class="info_box">
                        <div class="info_image"><img src="/resize/360/244/?img={{urlencode($item->image)}}"></div>
                        <div class="info_content">
                            <div class="info_title" style="height: 30px; overflow-y: hidden;">{{$item->title}}</div>
                            <div class="info_text" style="height: 80px; overflow-y: hidden;">{!! $item->preview !!}</div>
                            <div class="button info_button"><a href="/page/{{ $item->slug }}"><span>Читати ще</span><span>Читати ще</span></a></div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>

    <!-- CTA -->

    <div class="cta">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/cta_1.jpg" data-speed="0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="cta_container d-flex flex-xl-row flex-column align-items-xl-start align-items-center justify-content-xl-start justify-content-center">
                        <div class="cta_content text-xl-left text-center">
                            <div class="cta_title">Медичний портал</div>
                            <div class="cta_subtitle">Запишіться на прийом до одного з наших професійних лікарів, використовуючи особистий кабінет</div>
                        </div>
                        <div class="button cta_button ml-xl-auto"><a href="http://ks.medportal.ua" target="_blank"><span>перейти</span><span>перейти</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Services -->
{{-- 
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title">Наші послуги</div>
                </div>
            </div>
            <div class="row icon_boxes_row">
                
                <!-- Icon Box -->
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="images/icon_1.svg" alt=""></div>
                            <div class="icon_box_title">Консультація терапевта</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="images/icon_2.svg" alt=""></div>
                            <div class="icon_box_title">Консультація педіатра</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-xl-4 col-lg-6">
                    <div class="icon_box">
                        <div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="icon_box_icon"><img src="images/icon_3.svg" alt=""></div>
                            <div class="icon_box_title">Консультація сімейного лікаря</div>
                        </div>
                        <div class="icon_box_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lorem maximus malesuada lorem maximus mauris.</div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="button services_button ml-auto mr-auto"><a href="#"><span>Читати ще</span><span>Читати ще</span></a></div>
                </div>
            </div>
        </div>
    </div>
 --}}
    <!-- Відділенняs -->

    <div class="departments">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title">Наші Відділення</div>
                    {{-- <div class="section_subtitle">вибрати з</div> --}}
                </div>
            </div>
            <div class="row dept_row">
                <div class="col">
                    <div class="dept_slider_container_outer">
                        <div class="dept_slider_container">
                            <div class="owl-carousel owl-theme dept_slider">
                            @foreach($departments as $item)
                                <div class="owl-item dept_item">
                                    <div class="dept_image"><img src="{{ $item->image }}" alt="{{ $item->title }}"></div>
                                    <div class="dept_content">
                                        <div class="dept_title"><a href="#dep_{{$item->id}}" data-toggle="modal">{{$item->title}}</a></div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>

                    @foreach($departments as $item)
                        <div class="modal fade" id="dep_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="dep_{{$item->id}}Label" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="dep_{{$item->id}}Label">{{$item->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                {!! $item->text !!}
                              </div>
                            </div>
                          </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>

    <!-- FAQ & News -->

    <div class="stuff">
        <div class="container">
            <div class="row">

                <!-- FAQ -->
                <div class="col-lg-7">
                    <div class="faq">
                        <div class="faq_title">Запитання та поради</div>
                        <div class="elements_accordions">
                            <div class="accordions">
                            @foreach($faqs as $item)
                                <div class="accordion_container">
                                    <div class="accordion d-flex flex-row align-items-center"><div>{{ $item->question }}</div></div>
                                    <div class="accordion_panel">
                                        <div>{!! $item->answer !!}</div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Latest News -->
                <div class="col-lg-5">
                    <div class="news">
                        <div class="news_title">Останні новини</div>
                        <div class="news_container">
                        @foreach($lastPages as $item)
                            <!-- Latest News Post -->
                            <div class="latest d-flex flex-row align-items-start justify-content-start">
                                <div><div class="latest_image"><img src="{{$item->image}}" alt="{{$item->title}}"></div></div>
                                <div class="latest_content">
                                    <div class="latest_title"><a href="/page/{{ $item->slug }}">{{$item->title}}</a></div>
                                    {{-- <div class="latest_info"> --}}
                                        {{-- {{$item->created_at}} --}}
                                        {{-- <ul class="d-flex flex-row align-items-start justify-content-start">
                                            <li><a href="#">by Jane Smith</a></li>
                                            <li><a href="#">April 25, 2018</a></li>
                                        </ul> --}}
                                    {{-- </div> --}}
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection