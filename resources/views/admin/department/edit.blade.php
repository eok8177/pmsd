@extends('admin.layout')

@section('content')
<div class="card">
  <div class="card-header bg-light">
    <h3>@lang('message.department')</h3>
  </div>

  <div class="card-body">

    {!! Form::open(['route' => ['admin.department.update', $page->id], 'method' => 'PUT']) !!}
      @include('admin.department.form')
    {!! Form::close() !!}

  </div>
</div>

@endsection
