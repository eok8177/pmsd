
<div class="form-group row">
  <label for="question" class="col-sm-2 col-form-label">@lang('message.question')</label>
  <div class="col-sm-10">
    {!! Form::text('question', $page->question, ['class' => 'form-control']) !!}
  </div>
</div>


<div class="form-group row">
  <label for="answer" class="col-sm-2 col-form-label">@lang('message.answer')</label>
  <div class="col-sm-10">
    {!! Form::textarea('answer', $page->answer, ['class' => 'form-control', 'rows' => '2']) !!}
  </div>
</div>

<div class="toggle-switch" data-ts-color="primary">
  {!! Form::hidden('published', 0) !!}
  <label for="published" class="ts-label">@lang('message.published')</label>
  {!! Form::checkbox('published', 1, $page->published, ['id' => 'published']) !!}
  <label for="published" class="ts-helper"></label>
</div>

<hr>


<button type="submit" class="btn btn-success">
  <i class="fa fa-download"></i> &nbsp; @lang('message.save')
</button>