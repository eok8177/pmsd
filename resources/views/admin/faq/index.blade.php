@extends('admin.layout')

@section('content')

<div class="card">
  <div class="card-header bg-light">
    <h3>@lang('message.faq')</h3>
    <div>
      <a href="{{ route('admin.faq.create') }}" class="btn btn-light"><i class="fa fa-plus-square-o"></i> @lang('message.create')</a>
    </div>
  </div>

  <div class="card-body" style="position: relative;">
    <div class="table-responsive">
      <table class="table text-center sorted_table" model="Faq">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">actions</th>
            <th scope="col">@lang('message.question')</th>
            <th scope="col">@lang('message.published')</th>
          </tr>
        </thead>
        <tbody>

          @foreach($page as $item)
          <tr data-id="{{$item->id}}">
            <th scope="row">{{$item->id}}</th>
            <td>
              <a href="{{ route('admin.faq.edit', $item->id) }}" title="Edit"><i class="fa fa-edit"></i></a>
              &nbsp;&nbsp;&nbsp;
              <a href="{{ route('admin.faq.destroy', $item->id) }}" title="Delete" class="delete-item"><i class="fa fa-trash-o"></i></a>
            </td>
            <td>{{$item->question}}</td>
            <td>
              <a href="{{route('admin.ajax.status', ['id' => $item->id, 'model' => 'Page', 'field' => 'published'])}}" class="status fa fa-{{$item->published ? 'check-circle' : 'times-circle'}}"></a>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>

</div>

@endsection