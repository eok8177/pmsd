@extends('admin.layout')

@section('content')
<div class="card">
  <div class="card-header bg-light">
    <h3>@lang('message.faq')</h3>
  </div>

  <div class="card-body">

    {!! Form::open(['route' => ['admin.faq.update', $page->id], 'method' => 'PUT']) !!}
      @include('admin.faq.form')
    {!! Form::close() !!}

  </div>
</div>

@endsection
