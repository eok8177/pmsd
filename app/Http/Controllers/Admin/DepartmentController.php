<?php

namespace App\Http\Controllers\Admin;

use App\Model\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    public function index()
    {
        return view('admin.department.index', [
            'page' => Department::orderBy('order')->get()
        ]);
    }

    public function create()
    {
        return view('admin.department.create', ['page' => new Department]);
    }

    public function store(Request $request, Department $department)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $department = $department->create($request->all());

        return redirect()->route('admin.department.index')->with('success', 'Item created');
    }

    public function show(Department $department)
    {
        return view('admin.department.show', ['page' => $department]);
    }

    public function edit(Department $department)
    {
        return view('admin.department.edit', ['page' => $department]);
    }

    public function update(Request $request, Department $department)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $data = $request->all();

        $department->update($data);

        if (array_key_exists('redirect', $data)) return redirect()->route('admin.dashboard')->with('success', 'Item updated');

        return redirect()->route('admin.department.index')->with('success', 'Item updated');
    }

    public function destroy(Department $department)
    {
        $department->delete();

        return 'success';
    }
}
