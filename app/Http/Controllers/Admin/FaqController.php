<?php

namespace App\Http\Controllers\Admin;

use App\Model\Faq;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index()
    {
        return view('admin.faq.index', [
            'page' => Faq::orderBy('order')->get()
        ]);
    }

    public function create()
    {
        return view('admin.faq.create', ['page' => new Faq]);
    }

    public function store(Request $request, Faq $faq)
    {
        $request->validate([
            'question' => 'required',
        ]);

        $faq = $faq->create($request->all());

        return redirect()->route('admin.faq.index')->with('success', 'Item created');
    }

    public function show(Faq $faq)
    {
        return view('admin.faq.show', ['page' => $faq]);
    }

    public function edit(Faq $faq)
    {
        return view('admin.faq.edit', ['page' => $faq]);
    }

    public function update(Request $request, Faq $faq)
    {
        $request->validate([
            'question' => 'required',
        ]);

        $data = $request->all();

        $faq->update($data);

        if (array_key_exists('redirect', $data)) return redirect()->route('admin.dashboard')->with('success', 'Item updated');

        return redirect()->route('admin.faq.index')->with('success', 'Item updated');
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();

        return 'success';
    }
}
