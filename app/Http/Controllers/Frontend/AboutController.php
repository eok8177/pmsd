<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Model\Doctor;

class AboutController extends Controller
{
    public function index()
    {

        return view('about', [
            'doctors' => Doctor::wherePublished(1)->orderBy('order','asc')->get(),
        ]);
    }
}
