<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Model\Page;
use App\Model\Department;
use App\Model\Faq;

class HomeController extends Controller
{
    public function index()
    {

        return view('home', [
            'pages' => Page::wherePublished(1)->where('show_on_main', 1)->orderBy('order','asc')->limit(3)->get(),
            'departments' => Department::wherePublished(1)->orderBy('order','asc')->get(),
            'faqs' => Faq::wherePublished(1)->orderBy('order','asc')->get(),
            'lastPages' => Page::wherePublished(1)->orderBy('created_at','desc')->limit(3)->get(),
        ]);
    }
}
