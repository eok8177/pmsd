<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Model\PageCategory;
use App\Model\Page;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::whereSlug($slug)->wherePublished(1)->firstOrFail();

        return view('page', [
            'page' => $page,
            'categories' => PageCategory::wherePublished(1)->get(),
            'lastPages' => Page::where('category_id', $page->category_id)->where('id', '!=' , $page->id)->wherePublished(1)->orderBy('created_at','desc')->limit(3)->get(),
        ]);
    }

    public function category($slug)
    {
        $pageCategory = PageCategory::whereSlug($slug)->wherePublished(1)->firstOrFail();

        return view('news', [
            'categories' => PageCategory::wherePublished(1)->get(),
            'pages' => $pageCategory->pages()->get(),
            'lastPages' => Page::wherePublished(1)->orderBy('created_at','desc')->limit(3)->get(),
        ]);
    }
}
