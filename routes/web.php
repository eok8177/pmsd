<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/contact', function () {
    return view('contact');
});
Route::get('/elements', function () {
    return view('elements');
});
Route::get('/services', function () {
    return view('services');
});


Route::group(['as' => 'frontend', 'namespace' => 'Frontend'], function() {
  Route::get('/', 'HomeController@index');
  Route::get('/about', 'AboutController@index');

  Route::get('/pages/{slug}', 'PageController@category');
  Route::get('/page/{slug}', 'PageController@show');

  // Route::get('/contacts', 'ContactController@index');

});



Auth::routes();

// Social login
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');

//Image resize & crop on view:  http://image.intervention.io/
Route::get('/resize/{w}/{h}',function($w=null, $h=null){
  $img = Illuminate\Support\Facades\Input::get("img");
  return \Image::make(public_path(urldecode($img)))->fit($w, $h, function ($constraint) {
      $constraint->upsize();
  })->response('jpg');
 });

// Admin
Route::group(['as' => 'admin.', 'middleware' => 'roles','roles' =>['admin', 'sadmin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function() {

    App::setLocale('ua');

    Route::put('ajax/status', ['as' => 'ajax.status', 'uses' => 'AjaxController@status']);
    Route::put('ajax/reorder', ['as' => 'ajax.reorder', 'uses' => 'AjaxController@reorder']);

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::resource('user', 'UserController');
    Route::resource('page-category', 'PageCategoryController');
    Route::resource('page', 'PageController');
    Route::resource('department', 'DepartmentController');
    Route::resource('doctor', 'DoctorController');
    Route::resource('faq', 'FaqController');
});